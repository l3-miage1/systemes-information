DROP TABLE Occupation; -- FK Seance + Creneau

DROP TABLE IF EXISTS DecalageExterne; -- FK ModuleEns
DROP TABLE IF EXISTS Assistance; -- FK ModuleEns + Annee
DROP TABLE IF EXISTS Repartition; -- FK Groupe + Etudiant
DROP TABLE IF EXISTS Cours; -- FK Groupe + ModuleEns + Enseignant
DROP TABLE IF EXISTS Volume; -- FK TypeSeance + ModuleEns
DROP TABLE IF EXISTS DecalageInterne; -- FK TypeSeance + ModuleEns
DROP TABLE IF EXISTS Seance; -- FK Groupe + Salle


DROP TABLE IF EXISTS Composition;-- FK Annee + Semestre
DROP TABLE IF EXISTS Etudiant; -- FK Annee
DROP TABLE IF EXISTS ModuleEns; -- FK Semestre
DROP TABLE IF EXISTS Groupe; -- FK TypeSeance
DROP TABLE IF EXISTS Specificite; -- FK Salle + TypeSeance
DROP TABLE IF EXISTS Affectation; -- FK Salle + Creneau

DROP TABLE IF EXISTS Annee;
DROP TABLE IF EXISTS Semestre;
DROP TABLE IF EXISTS TypeSeance;
DROP TABLE IF EXISTS Salle;
DROP TABLE IF EXISTS Creneau;
DROP TABLE IF EXISTS Enseignant;

-- Groupe 1 --------------------------------------

CREATE TABLE Annee
(
    idAnnee VARCHAR2(8) CONSTRAINT PK_Annee PRIMARY KEY,
    numAnnee NUMBER(1),
    niveau VARCHAR2(8) CHECK (niveau IN ('licence','master','doctorat')) ,
    mention VARCHAR2(32),
    parcours VARCHAR2(32)
);
CREATE TABLE Semestre
(
    numSemestre NUMBER(2) CONSTRAINT PK_Semestre PRIMARY KEY
);
CREATE TABLE TypeSeance
(
    typeSeance VARCHAR2(8) CHECK (typeSeance IN ('Cours','TD','TP','Exam')) CONSTRAINT PK_TypeSeance PRIMARY KEY,
);
CREATE TABLE Salle
(
    nomSalle VARCHAR2(16) CONSTRAINT PK_Salle PRIMARY KEY,
    capacite NUMBER(4),
    localisation VARCHAR2(32)
);
CREATE TABLE Creneau
(
    idCreneau VARCHAR2(8) CONSTRAINT PK_Creneau PRIMARY KEY,
    dateCreneau DATE,
    heureDebut DATE,
    jourSemaine VARCHAR2(8)
);
CREATE TABLE Enseignant
(
    idEnseignant VARCHAR2(8) CONSTRAINT PK_Enseignant PRIMARY KEY,
    nomEnseignant VARCHAR2(20),
    prenomEnseignant VARCHAR2(20),
    eMailEnseignant VARCHAR2(32)
);

-- Groupe 2 --------------------------------------

CREATE TABLE Composition
(
    numSemestre NUMBER(2),
    idAnnee VARCHAR2(8),
    CONSTRAINT FK_Composition_Annee_numSemestre FOREIGN KEY (numSemestre) REFERENCES Semestre(numSemestre),
    CONSTRAINT FK_Composition_Annee_idAnnee FOREIGN KEY (idAnnee) REFERENCES Annee(idAnnee),
    CONSTRAINT PK_Composition PRIMARY KEY (numSemestre, idAnnee)
);
CREATE TABLE Etudiant
(
    numEtudiant NUMBER(8) CONSTRAINT PK_Etudiant PRIMARY KEY,
    nomEtudiant VARCHAR2(20),
    prenomEtudiant VARCHAR2(20),
    idAnnee VARCHAR2(8),
    CONSTRAINT FK_Etudiant_Annee_idAnnee FOREIGN KEY (idAnnee) REFERENCES Annee(idAnnee)
);
CREATE TABLE ModuleEns
(
    idModule VARCHAR2(8) CONSTRAINT PK_ModuleEns PRIMARY KEY,
    intituleModule VARCHAR2(32),
    numSemestre NUMBER(2),
    CONSTRAINT FK_ModuleEns_Semestre_numSemestre FOREIGN KEY (numSemestre) REFERENCES Semestre(numSemestre)
);
CREATE TABLE Groupe
(
    typeSeance VARCHAR2(8) NOT NULL,
    numGroupe NUMBER(1) NOT NULL,
    CONSTRAINT FK_Groupe_TypeSeance_typeSeance FOREIGN KEY (typeSeance) REFERENCES TypeSeance(typeSeance),
    CONSTRAINT PK_Groupe PRIMARY KEY (typeSeance, numGroupe)
);
CREATE TABLE Specificite
(
    nomSalle VARCHAR2(16),
    typeSeance VARCHAR2(8),
    CONSTRAINT FK_Specificite_Salle_nomSalle FOREIGN KEY (nomSalle) REFERENCES Salle(nomSalle),
    CONSTRAINT FK_Specificite_TypeSeance_typeSeance FOREIGN KEY (typeSeance) REFERENCES TypeSeance(typeSeance),
    CONSTRAINT PK_Specificite PRIMARY KEY (nomSalle, typeSeance)
);
CREATE TABLE Affectation
(
    nomSalle VARCHAR2(16),
    idCreneau VARCHAR2(8),
    CONSTRAINT FK_Affectation_Salle_nomSalle FOREIGN KEY (nomSalle) REFERENCES Salle(nomSalle),
    CONSTRAINT FK_Affectation_Creneau_idCreneau FOREIGN KEY (idCreneau) REFERENCES Creneau(idCreneau),
    CONSTRAINT PK_Affectation PRIMARY KEY (nomSalle, idCreneau)
);

-- Groupe 3 --------------------------------------

CREATE TABLE DecalageExterne
(
    idModulePrec VARCHAR2(8),
    idModuleSucc VARCHAR2(8),
    typeDecalage VARCHAR2(32) CHECK (typeSeance IN ('Avec chevauchement','Sans chevauchement')),
    CONSTRAINT FK_DecalageExterne_ModuleEns_idModulePrec FOREIGN KEY (idModulePrec) REFERENCES ModuleEns(idModule),
    CONSTRAINT FK_DecalageExterne_ModuleEns_idModuleSucc FOREIGN KEY (idModuleSucc) REFERENCES ModuleEns(idModule),
    CONSTRAINT PK_DecalageExterne PRIMARY KEY (idModulePrec, idModuleSucc)
);
CREATE TABLE Assistance
(
    idModule VARCHAR2(8),
    idAnnee VARCHAR2(8),
    CONSTRAINT FK_Assistance_ModuleEns_idModule FOREIGN KEY (idModule) REFERENCES ModuleEns(idModule),
    CONSTRAINT FK_Assistance_Annee_idAnnee FOREIGN KEY (idAnnee) REFERENCES Annee(idAnnee),
    CONSTRAINT PK_Assistance PRIMARY KEY (idModule, idAnnee)
);
CREATE TABLE Repartition
(
    typeSeance VARCHAR2(8),
    numGroupe NUMBER(1),
    numEtudiant NUMBER(8),
    CONSTRAINT FK_Repartition_Groupe_typeSeance_numGroupe FOREIGN KEY (typeSeance, numGroupe) REFERENCES Groupe(typeSeance, numGroupe),
    CONSTRAINT FK_Repartition_Etudiant_numEtudiant FOREIGN KEY (numEtudiant) REFERENCES Etudiant(numEtudiant),
    CONSTRAINT PK_Repartition PRIMARY KEY (typeSeance, numGroupe, numEtudiant)
);
CREATE TABLE Cours
(
    idModule VARCHAR2(8),
    typeSeance VARCHAR2(8),
    numGroupe NUMBER(1),
    idEnseignant VARCHAR2(8),
    CONSTRAINT FK_Cours_ModuleEns_idModule FOREIGN KEY (idModule) REFERENCES ModuleEns(idModule),
    CONSTRAINT FK_Cours_Groupe_typeSeance_numGroupe FOREIGN KEY (typeSeance, numGroupe) REFERENCES Groupe(typeSeance, numGroupe),
    CONSTRAINT FK_Cours_Enseignant_idEnseignant FOREIGN KEY (idEnseignant) REFERENCES Enseignant(idEnseignant),
    CONSTRAINT PK_Cours PRIMARY KEY (idModule, typeSeance, numGroupe)
);
CREATE TABLE Volume
(
    idModule VARCHAR2(8),
    typeSeance VARCHAR2(8),
    volumeHoraire NUMBER(4),
    CONSTRAINT FK_Volume_ModuleEns_idModule FOREIGN KEY (idModule) REFERENCES ModuleEns(idModule),
    CONSTRAINT FK_Volume_TypeSeance_typeSeance FOREIGN KEY (typeSeance) REFERENCES TypeSeance(typeSeance),
    CONSTRAINT PK_Volume PRIMARY KEY (idModule, typeSeance)
);
CREATE TABLE DecalageInterne
(
    idModule VARCHAR2(8),
    typeSeanceApres VARCHAR2(8),
    typeSeanceAvant VARCHAR2(8),
    valeurDecalage NUMBER(2),
    CONSTRAINT FK_DecalageInterne_ModuleEns_idModule FOREIGN KEY (idModule) REFERENCES ModuleEns(idModule),
    CONSTRAINT FK_DecalageInterne_TypeSeance_typeSeanceApres FOREIGN KEY (typeSeanceApres) REFERENCES TypeSeance(typeSeance),
    CONSTRAINT FK_DecalageInterne_TypeSeance_typeSeanceAvant FOREIGN KEY (typeSeanceAvant) REFERENCES TypeSeance(typeSeance),
    CONSTRAINT PK_DecalageInterne PRIMARY KEY (idModule, typeSeanceApres, typeSeanceAvant)
);
CREATE TABLE Seance
(
    idModule VARCHAR2(8),
    typeSeance VARCHAR2(8),
    numGroupe NUMBER(1),
    numOrdre NUMBER(2),
    decalageDebut NUMBER(2),
    dureeEffective NUMBER(2),
    etatSeance VARCHAR2(32) CHECK (etatSeance IN ('A planifier','Planifiée','Annulée','Effectuée')) ,
    nomSalle VARCHAR2(16),
    CONSTRAINT FK_Seance_ModuleEns_idModule FOREIGN KEY (idModule) REFERENCES ModuleEns(idModule),
    CONSTRAINT FK_Seance_Groupe_typeSeance_numGroupe FOREIGN KEY (typeSeance, numGroupe) REFERENCES Groupe(typeSeance, numGroupe),
    CONSTRAINT FK_Seance_Salle_nomSalle FOREIGN KEY (nomSalle) REFERENCES Salle(nomSalle),
    CONSTRAINT PK_Seance PRIMARY KEY (idModule, typeSeance, numGroupe, numOrdre)
);

-- Groupe 4 --------------------------------------

CREATE TABLE Occupation
(
    idModule VARCHAR2(8),
    typeSeance VARCHAR2(8),
    numGroupe NUMBER(1),
    numOrdre NUMBER(2),
    idCreneau VARCHAR2(8),
    CONSTRAINT FK_Occupation_Seance_idModule_typeSeance_numGroupe_numOrdre FOREIGN KEY (idModule, typeSeance, numGroupe, numOrdre) REFERENCES Seance(idModule, typeSeance, numGroupe, numOrdre),
    CONSTRAINT FK_Occupation_Creneau_idCreneau FOREIGN KEY (idCreneau) REFERENCES Creneau(idCreneau),
    CONSTRAINT PK_Occupation PRIMARY KEY (idModule, typeSeance, numGroupe, numOrdre, idCreneau)
);