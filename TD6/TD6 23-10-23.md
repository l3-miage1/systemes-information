# Exo2 PL/SQL (feuille seulement)


### 0. Ecrire une fonction NbAlbumPublic qui prend en entrée un *email de client* existant et qui renvoie le nombre total d'albums de visibilité publique du client.
```SQL
CREATE OR REPLACE FUNCTION NbAlbumPublic(emailCli IN VARCHAR2) RETURN NUMBER
As
	nb number;
BEGIN
	SELECT count(*) INTO nb FROM Album WHERE emailClient = emailCli AND visibilite='Public';
	RETURN nb;
END NbAlbumPublic;
/
```


### 1. Ecrire une fonction *NbImage* qui prend en entrée un *email de client* existant et qui renvoie le nombre total d'images contenues dans l'ensemble des albums client.
```SQL 
CREATE OR REPLACE Function NbImage(email IN VARCHAR) RETURN NUMBER
Is nb NUMBER
BEGIN
	SELECT COUNT(*) INTO nb FROM Album NATURAL JOIN
		Image WHERE emailClient=eamil;
	RETURN nb;
END nbImage;
```

### 2
```sql
CREATE OR REPLACE Function Occupe(email IN VARCHAR) RETURN NUMBER
Is espaceOccupee
BEGIN
	SELECT NVL(SUM(taille), 0) INTO espaceOccupee FROM Image NATURAL JOIN (
		SELECT idAlbum FROM Album WHERE eMailClient=email
	);
	RETURN espaceOccupee;
END Occupe;
```

### 3
```SQL
ALTER TABLE Client ADD (nombreImage NUMBER(4), espaceStockageDispo NUMBER(4))

DECLARE
	CURSOR C1 IS SELECT * FROM Client FOR UPDATE OF(nombreImage, espaceStockageDispo);
BEGIN
	FOR Cli in C1 LOOP
		UPDATE Client SET ([Cli.]nombreImage=nbImage(Cli.eMailUtilisateur), [Cli.]espaceStockageDispo=Cli.quota-Occupe(Cli.eMailUtilisateur))
		WHERE CURRENT OF C1;
	END LOOP;
END;
```

### 4
```SQL
CREATE OR REPLACE TRIGGER 

`CREATE TRIGGER trigger_name   {BEFORE | AFTER} {INSERT | UPDATE| DELETE }   ON table_name FOR EACH ROW   trigger_body;`
```

### 7
```SQL
CREATE OR REPLACE ReductionCommande(numCmd Commande.numCommande%TYPE ,idLabo Laboratoire.idLaboratoire%TYPE) RETURN NUMBER
Is 
	reduCommande NUMBER(4,2);
	quantiteCommander NUMBER;
	qteMax TarifDegressif.quantiteMaxi%TYPE;
	qteMin TarifDegressif.quantiteMini%TYPE;
BEGIN
	SELECT sum(quantite) INTO quantiteCommander FROM TirageImages WHERE numCommande=numCmd;
	SELECT quantiteMini, quantiteMaxi INTO qteMin, qteMax FROM TarifDegressif WHERE idLaboratoire=idLabo;
	IF (quantiteCommander BETWEEN qteMin AND qteMax) THEN
		SELECT reduction INTO reduCommande FROM TarifDegressif WHERE idLaboratoire=idLabo;
	ELSE 
		reduCommande = 0;
	END IF;
	RETURN reduCommande;
END ReductionCommande;
```

### 8
```SQL


```