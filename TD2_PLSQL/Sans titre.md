# 1
### 0. Ecrire une fonction NbAlbumPublic qui prend en entrée un *email de client* existant et qui renvoie le nombre total d'albums de visibilité publique du client.
```SQL
CREATE OR REPLACE FUNCTION NbAlbumPublic(emailCli IN VARCHAR2) RETURN NUMBER
As
	nb number;
BEGIN
	SELECT count(*) INTO nb FROM Album WHERE emailClient = emailCli AND visibilite='Public';
	RETURN nb;
END NbAlbumPublic;
/
```
