# TD3 (Bon ok c'est carrément le TD2)

## REQUETES
1. Les noms des enseignants qui sont compétents en ORACLE mais pas en JAVA
```sql

```

2. La moyenne d’age des enseignants
```sql
SELECT AVG(DateDiff('yyyy', date_naiss, SYSDATE)) FROM Formateur;
-- or
SELECT AVG(TO_CHAR(sysdate, 'YYYY')-TO_CHAR(date_naiss, 'YYYY')) FROM Formateur;
```
### -------------------------------------------
### TIPS
- Pour les view :
```sql
CREATE or REPLACE VIEW nomView nom_view(id_ens, age)
    SELECT id_ens, TO_CHAR(SYSDATE, 'YYYY')-TO_CHAR(date_naiss, 'YYYY') AS age FROM Formateur;
```
### -------------------------------------------

3. Le nom de l’enseignant le plus jeune
```sql
SELECT nom, prenom FROM Formateur where date_naiss=(
    SELECT MAX(date_naiss) FROM Formateur
);
```

4. Les noms des enseignants qui ont signé un contrat avec une société qui ne propose pas de formation ORACLE
```sql
SELECT nom FROM Formateur minus(
    (SELECT * FROM Contrats) natural join
    (SELECT * FROM Formation where matiere == 'ORACLE')
);
```

5. Les noms des enseignants qui sont compétents dans au moins trois matières 
```sql
SELECT nom, prenom FROM(
    (SELECT * FROM FORMATEUR)
    natural join
    (SELECT id_Enseignant FROM Competence 
    GROUP BY (id_Enseignant) 
    HAVING count(*) >= 3)
);
```

6. Les couples (nom1, nom2) d’enseignants qui sont compétents pour la même matière
    - Ne pas utilser l'inegalite pour evité les doublons, utiliser plutot le inferieur ou superieur.
```sql
-- v1.0
-- SELECT nom, nom FROM (SELECT * FROM Formateur natural join Competence AS FC1) join (SELECT * FROM Formateur natural join Competence AS FC2) on FC1.matiere = FC2.matiere where FC1.id_Enseignant < FC2.id_Enseignant;

-- v2.0 surement pas optimisé
SELECT nom1, nom2 FROM(
    (SELECT nom AS nom1, matiere FROM Formateur natural join Competence)
    natural join
    (SELECT nom AS nom2, matiere FROM Formateur natural join Competence))
    WHERE nom1 < nom2 GROUP BY nom1, nom2
;
```

7. Pour chaque matière le nombre d’enseignants qui sont compétents sur cette matière
```sql
SELECT matiere, count(id_Enseignant) as Nombre_Enseignant FROM Competence GROUP BY (matiere);
```

8. Matières proposées en formation dans au moins une société 
```sql
-- sans DISTINCT (enfin je crois)
SELECT matiere as matiere_proposer FROM Formation;
```

9. Matières proposées en formation par aucune société
```sql
SELECT matiere FROM Competence MINUS(
    SELECT matiere FROM Formation
);
```

10. Matières proposées en formation par toutes les sociétés
```sql
SELECT matiere FROM Formation GROUP BY matiere HAVING count(*) = (
    SELECT count(DISTINCT(societe)) FROM Formation
);
```

11. Matières proposées en formation seulement par IBM
```sql
-- matiere proposer par IBM
SELECT matiere FROM Formation WHERE societe = 'IBM' 
MINUS
-- matiere proposer par les autres société que IBM
(SELECT matiere FROM Formation WHERE societe <> 'IBM');
```

12. Matières proposées en formation au plus par IBM
```sql
SELECT matiere FROM Formation WHERE societe = 'IBM' INTERSECT(
    SELECT matiere FROM Formation WHERE societe <> 'IBM'
);
```

13. Matières proposées en formation par au moins deux sociétés 
```sql
-- A moi
SELECT matiere FROM Formation GROUP BY matiere HAVING count(*) >= 2;
-- Correction alternative
SELECT DISTINCT F1.matiere FROM Formation F1, Formation F2 WHERE F1.matiere = F2.matiere and F1.societe <> F2.societe;
```

14. Matières proposées en formation par une et une seule société 
```sql
SELECT matiere FROM Formation GROUP BY matiere HAVING count(*) = 1;
```

15. Sociétés qui proposent une formation ORACLE et JAVA et qui ont pas de contrat avec Alain Fournier.
```sql
SELECT societe FROM Formation WHERE matiere = 'ORACLE' INTERSECT(
    SELECT societe FROM Formation WHERE matiere = 'JAVA'
) MINUS(
    SELECT societe FROM Contrats natural join Formateur WHERE nom = 'Fournier' and prenom = 'Alain'
);
```

16. Sociétés qui proposent des formations qui sont dans les compétences d’Alain Fournier.
```sql
SELECT societe FROM Formation WHERE matiere IN(
    SELECT matiere FROM Competence natural join Formateur WHERE where nom = 'Fournier' and prenom = 'Alain'
);
```

17. Sociétés qui ont signé un contrat avec Alain Fournier et Mélanie Lenoir.
```sql
SELECT societe FROM Contrats natural join Formateur WHERE nom = 'Fournier' and prenom = 'Alain' and societe in (
    SELECT societe FROM Contrats natural join Formateur WHERE nom = 'Lenoir' and prenom = 'Mélanie'
);
```

18. Sociétés qui proposent des formations qui sont dans les compétences de Mélanie Lenoir mais qui n’ont pas signé de contrat avec Alain Fournier.
```sql
(SELECT societe FROM Formation WHERE matiere IN(
    SELECT matiere FROM Competence natural join Formateur WHERE nom = 'Lenoir' and prenom = 'Mélanie'
)) 
MINUS
-- Tout les societe qui n'ont pas signer avec Alain
(SELECT societe FROM Contrats natural join Formateur WHERE nom = 'Fournier' and prenom = 'Alain');
```

19. Sociétés qui proposent des formations qui sont dans les compétences de Mélanie Lenoir et qui ont signé un contrat avec Alain Fournier.
```sql
SELECT societe FROM Formation WHERE matiere IN(
    SELECT matiere FROM Competence natural join Formateur WHERE nom = 'Lenoir' and prenom = 'Mélanie'
) INTERSECT(
    SELECT societe FROM Contrats natural join Formateur WHERE nom = 'Fournier' and prenom = 'Alain'
);
```

20. Sociétés qui proposent au moins une formation qui n’est pas dans les compétences de Mélanie Lenoir.
```sql
SELECT societe FROM Formation WHERE matiere NOT IN(
    SELECT matiere FROM Competence natural join Formateur WHERE nom = 'Lenoir' and prenom = 'Mélanie'
);
```

21. Sociétés qui proposent uniquement des formations qui sont seulement dans les compétences de Mélanie Lenoir (seulement = on ne les retrouve pas chez d’autres enseignants) 
```sql
SELECT societe FROM Formation WHERE matiere IN(
    SELECT matiere FROM Competence natural join Formateur WHERE nom = 'Lenoir' and prenom = 'Mélanie'
) MINUS(
    SELECT societe FROM Formation WHERE matiere IN(
        SELECT matiere FROM Competence natural join Formateur WHERE nom <> 'Lenoir' and prenom <> 'Mélanie'
    )
);
-- or
-- Creer une view avec les matieres de Melanie Lenoir
CREATE or REPLACE VIEW matiere_melanie AS
    SELECT matiere FROM Competence natural join Formateur WHERE nom = 'Lenoir' and prenom = 'Mélanie';
-- Puis faire la requete
SELECT societe FROM Formation WHERE matiere IN(
    SELECT matiere FROM matiere_melanie
) MINUS(
    SELECT societe FROM Formation WHERE matiere IN(
        SELECT matiere FROM Competence natural join Formateur WHERE nom <> 'Lenoir' and prenom <> 'Mélanie'
    )
);
```

22. Sociétés qui proposent des formations sur toutes les matières.
```sql
SELECT societe FROM Formation GROUP BY societe HAVING count(*) = (
    SELECT count(DISTINCT(matiere)) FROM Formation
);
```




## CREATION DE LA TABLE

### Definition de la base de données
```sql
DROP TABLE Formation;
DROP TABLE Contrats;
DROP TABLE Competence;
DROP TABLE Formateur;

CREATE TABLE Formateur
(
    id_Enseignant NUMBER(8) CONSTRAINT PK_Formateur PRIMARY KEY,
    nom VARCHAR2(50),
    prenom VARCHAR2(50),
    date_naiss DATE
);
CREATE TABLE Competence
(
    id_Enseignant NUMBER(8),
    matiere VARCHAR2(25),
    CONSTRAINT PK_Competence PRIMARY KEY (id_Enseignant, matiere),
    CONSTRAINT FK_Competence_Formateur_id_Enseignant FOREIGN KEY (id_Enseignant) REFERENCES Formateur(id_Enseignant)
);
CREATE TABLE Contrats
(
    id_Enseignant NUMBER(8),
    societe VARCHAR2(25),
    CONSTRAINT PK_Contrats PRIMARY KEY (id_Enseignant, societe),
    CONSTRAINT FK_Contrats_Formateur_id_Enseignant FOREIGN KEY (id_Enseignant) REFERENCES Formateur(id_Enseignant)
);
-- enfaite ces attributs la ne vienne pas des autres tables.
CREATE TABLE Formation
(
    matiere VARCHAR2(25),
    societe VARCHAR2(25),
    CONSTRAINT PK_Formation PRIMARY KEY (matiere, societe)
);
```

### Insertion de la base de données
```sql
-- insertion des formateurs
INSERT INTO Formateur VALUES (1, 'Dupont', 'Jean', TO_DATE('01/01/1980', 'DD/MM/YYYY'));
INSERT INTO Formateur VALUES (2, 'Durand', 'Pierre', TO_DATE('01/01/1985', 'DD/MM/YYYY'));
INSERT INTO Formateur VALUES (3, 'Martin', 'Paul', TO_DATE('01/01/1990', 'DD/MM/YYYY'));
INSERT INTO Formateur VALUES (4, 'Dubois', 'Jacques', TO_DATE('01/01/1995', 'DD/MM/YYYY'));
INSERT INTO Formateur VALUES (5, 'Lefebvre', 'Jeanne', TO_DATE('01/01/2000', 'DD/MM/YYYY'));
INSERT INTO Formateur VALUES (6, 'Lenoir', 'Mélanie', TO_DATE('01/01/2005', 'DD/MM/YYYY'));

-- insertion des compétences
INSERT INTO Competence VALUES (1, 'ORACLE');
INSERT INTO Competence VALUES (1, 'JAVA');
INSERT INTO Competence VALUES (1, 'C++');
INSERT INTO Competence VALUES (2, 'ORACLE');
INSERT INTO Competence VALUES (2, 'JAVA');
INSERT INTO Competence VALUES (3, 'ORACLE');
INSERT INTO Competence VALUES (3, 'C++');
INSERT INTO Competence VALUES (4, 'JAVA');
INSERT INTO Competence VALUES (4, 'C++');
INSERT INTO Competence VALUES (5, 'ORACLE');
INSERT INTO Competence VALUES (5, 'JAVA');
INSERT INTO Competence VALUES (5, 'C++');
INSERT INTO Competence VALUES (5, 'PYTHON');
INSERT INTO Competence VALUES (6, 'ORACLE');
INSERT INTO Competence VALUES (6, 'JAVA');
INSERT INTO Competence VALUES (6, 'C++');
INSERT INTO Competence VALUES (6, 'PYTHON');

-- insertion des contrats
INSERT INTO Contrats VALUES (1, 'IBM');
INSERT INTO Contrats VALUES (1, 'ORACLE');
INSERT INTO Contrats VALUES (2, 'IBM');
INSERT INTO Contrats VALUES (2, 'ORACLE');
INSERT INTO Contrats VALUES (3, 'IBM');
INSERT INTO Contrats VALUES (3, 'ORACLE');
INSERT INTO Contrats VALUES (4, 'IBM');
INSERT INTO Contrats VALUES (4, 'ORACLE');
INSERT INTO Contrats VALUES (5, 'IBM');
INSERT INTO Contrats VALUES (5, 'ORACLE');
INSERT INTO Contrats VALUES (6, 'IBM');
INSERT INTO Contrats VALUES (6, 'ORACLE');
INSERT INTO Contrats VALUES (6, 'PYTHON');


-- insertion des formations
INSERT INTO Formation VALUES ('ORACLE', 'IBM');
INSERT INTO Formation VALUES ('JAVA', 'IBM');
INSERT INTO Formation VALUES ('C++', 'IBM');
INSERT INTO Formation VALUES ('ORACLE', 'ORACLE');
INSERT INTO Formation VALUES ('JAVA', 'ORACLE');
INSERT INTO Formation VALUES ('C++', 'ORACLE');
INSERT INTO Formation VALUES ('PYTHON', 'ORACLE');
INSERT INTO Formation VALUES ('ORACLE', 'MICROSOFT');
INSERT INTO Formation VALUES ('JAVA', 'MICROSOFT');
INSERT INTO Formation VALUES ('C++', 'MICROSOFT');
INSERT INTO Formation VALUES ('PYTHON', 'MICROSOFT');
INSERT INTO Formation VALUES ('.NET', 'MICROSOFT');
```
