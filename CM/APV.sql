DROP TABLE IF EXISTS VOL;
DROP TABLE IF EXISTS AVION;
DROP TABLE IF EXISTS PILOTE;


CREATE TABLE PILOTE
(
    numPilote NUMBER(4) CONSTRAINT PK_PILOTE PRIMARY KEY,
    nomPilote VARCHAR2(20),
    adresse VARCHAR2(20)
);
CREATE TABLE AVION
(
    numAvion NUMBER(6) CONSTRAINT PK_AVION PRIMARY KEY,
    capacite NUMBER(3),
    localisation VARCHAR2(20)
);
CREATE TABLE VOL
(
    numVol VARCHAR2(6) CONSTRAINT PK_VOL PRIMARY KEY,
    villeDep VARCHAR2(20),
    villeArr VARCHAR2(20),
    heureDep DATE(12),
    heurreArr DATE(12),
    numPilote NUMBER(4),
    numAvion NUMBER(6),
    CONSTRAINT FK_VOL_PILOTE_numPilote FOREIGN KEY (numPilote) REFERENCES PILOTE(numPilote),
    CONSTRAINT FK_VOL_AVION_numAvion FOREIGN KEY (numAvion) REFERENCES AVION(numAvion)
);

-- faire les insertions *10
INSERT INTO PILOTE VALUES (1,'nom1','adresse1');
INSERT INTO PILOTE VALUES (2,'nom2','adresse2');
INSERT INTO PILOTE VALUES (3,'nom3','adresse3');
INSERT INTO PILOTE VALUES (4,'nom4','adresse4');
INSERT INTO PILOTE VALUES (5,'nom5','adresse5');
INSERT INTO PILOTE VALUES (6,'nom6','adresse6');
INSERT INTO PILOTE VALUES (7,'nom7','adresse7');
INSERT INTO PILOTE VALUES (8,'nom8','adresse8');
INSERT INTO PILOTE VALUES (9,'nom9','adresse9');
INSERT INTO PILOTE VALUES (10,'nom10','adresse10');

INSERT INTO AVION VALUES (1,100,'localisation1');
INSERT INTO AVION VALUES (2,200,'localisation2');
INSERT INTO AVION VALUES (3,300,'localisation3');
INSERT INTO AVION VALUES (4,400,'localisation4');
INSERT INTO AVION VALUES (5,500,'localisation5');
INSERT INTO AVION VALUES (6,600,'localisation6');
INSERT INTO AVION VALUES (7,700,'localisation7');
INSERT INTO AVION VALUES (8,800,'localisation8');
INSERT INTO AVION VALUES (9,900,'localisation9');
INSERT INTO AVION VALUES (10,1000,'localisation10');

INSERT INTO VOL VALUES ('vol1','villeDep1','villeArr1',TO_DATE('01/01/2019 12:00:00','DD/MM/YYYY HH24:MI:SS'),TO_DATE('01/01/2019 13:00:00','DD/MM/YYYY HH24:MI:SS'),1,1);
INSERT INTO VOL VALUES ('vol2','villeDep2','villeArr2',TO_DATE('02/01/2019 12:00:00','DD/MM/YYYY HH24:MI:SS'),TO_DATE('02/01/2019 13:00:00','DD/MM/YYYY HH24:MI:SS'),2,2);
INSERT INTO VOL VALUES ('vol3','villeDep3','villeArr3',TO_DATE('03/01/2019 12:00:00','DD/MM/YYYY HH24:MI:SS'),TO_DATE('03/01/2019 13:00:00','DD/MM/YYYY HH24:MI:SS'),3,3);
INSERT INTO VOL VALUES ('vol4','villeDep4','villeArr4',TO_DATE('04/01/2019 12:00:00','DD/MM/YYYY HH24:MI:SS'),TO_DATE('04/01/2019 13:00:00','DD/MM/YYYY HH24:MI:SS'),4,4);
INSERT INTO VOL VALUES ('vol5','villeDep5','villeArr5',TO_DATE('05/01/2019 12:00:00','DD/MM/YYYY HH24:MI:SS'),TO_DATE('05/01/2019 13:00:00','DD/MM/YYYY HH24:MI:SS'),5,5);
INSERT INTO VOL VALUES ('vol6','villeDep6','villeArr6',TO_DATE('06/01/2019 12:00:00','DD/MM/YYYY HH24:MI:SS'),TO_DATE('06/01/2019 13:00:00','DD/MM/YYYY HH24:MI:SS'),6,6);
INSERT INTO VOL VALUES ('vol7','villeDep7','villeArr7',TO_DATE('07/01/2019 12:00:00','DD/MM/YYYY HH24:MI:SS'),TO_DATE('07/01/2019 13:00:00','DD/MM/YYYY HH24:MI:SS'),7,7);