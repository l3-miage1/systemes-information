-- Définir un tableau dans lequel on mettra les numéro de pilote. Vous devez donc définir le
-- même type que celui des numéros de pilote en utilisant%TYPE
-- Affecter à la première case de ce tableau la valeur 12
DECLARE
    nb_vol_pilote NUMBER;
    reponse VARCHAR2;
BEGIN
    SELECT count(*) into nb_vol_pilote FROM VOL where numPilote=1;
    IF nb_vol_pilote > 50 THEN
        reponse := 'Senior';
    ELSIF  51 > nb_vol_pilote > 20 THEN
        reponse := 'Confirmé';
    ELSIF nb_vol_pilote < 21 THEN
        reponse := 'debutant';
    ELSE THEN
        reponse := 'en attente de vol';
    END IF;
    dbms_outpout.put_line(reponse);
END;
/

-- Y a t il des pilotes sans Adresse ? (reponse Oui ou Non)
DECLARE
    nb_pilote_sans_adresse NUMBER;
    msg VARCHAR2(3);
BEGIN
    SELECT count(*) into nb_pilote_sans_adresse FROM PILOTE where adresse IS NULL;
    IF nb_pilote_sans_adresse > 0 THEN
        msg := 'Oui';
    ELSE THEN
        msg := 'Non';
    END IF;
    dbms_outpout.put_line(msg);
END;
/


-- Créez une table piloteNonActif qui contient le numéro et nom du pilote
-- Lire un numpilote X. Si le pilote n’a fait aucun vol alors le rajouter dans la table piloteNonActif
-- puis écrire :
--  - soit « pilote inactif, il y a donc au total x pilote inactif » où x est le nombre de pilote inactif
--  - soit « pilote actif ».

Create table piloteNonActif (
    numPilote number(4) primary key
    ,nomPilote varchar2(20)
) ;
accept numP prompt 'donner pilote'; -- pour lire un numéro de pilote dans numP
declare
    nbVol number ; -- pour calculer le nb de vol
    NbInActif number ; -- pour calculer le nombre de pilote inactif
    monPilote Pilote%ROWTYPE ;
begin
    select count(numVol) into nbVol from Vol where numPilote=&numP ;
    IF nbVol= 0 THEN
        select * into monPilote from Pilote where numPilote=&numP ;
        insert into piloteNonActif(numPilote,nomPilote)
        values(monPilote.numPilote,monPilote.nomPilote) ;
        select count(*) into nbInActif from piloteNonActif ;
        dbms_output.put_line('Pilote inactif, il y a donc au total '|| nbInActif || ' pilotes inactifs') ;
    ELSE
        dbms_output.put_line('Pilote actif') ;
END IF ;
end;
/

-- 65
DECLARE
    CURSOR C1 IS SELECT numPilote, count(numVol) as nbVol FROM VOL GROUP BY numPilote HAVING(count(numVol) < 10);
BEGIN
    FOR pilote, nbVol in C1 LOOP
        DELETE FROM Pilote where numPilote=pilote.numPilote;
        dbms_outpout.put_line('Pilote ' || pilote.numPilote || pilote.nbVol)
    END LOOP;
end;
/


-- 67
DECLARE
    CURSOR  C1 is SELECT numAvion 
