### CURRENT OF

![[cours_PLSQL.pdf#page=80]]

```SQL
DECLARE
	CURSOR C IS SELECT id FROM Drh WHERE grade=1 FOR UPDATE grade;
BEGIN
	For Empl in C1 LOOP
		UPDATE Drh set grade=2 WHERE CURRENT OF C;
	END LOOP;
END;
/
```

## Fonctions & Procédures
![[cours_PLSQL.pdf#page=85]]
* Ne renvoie pas la ligne de l'erreur
* Quand on créée une function le DECLARE -> AS

![[cours_PLSQL.pdf#page=90]]
```SQL
-- 1)
CREATE OR REPLACE FUNCTION nbVolEffectue (nomP IN VARCHAR2) RETURN NUMBER
As
	nbVol NUMBER;
BEGIN
	SELECT count(numVol) INTO nbVol FROM VOL natural join (SELECT * FROM Pilote WHERE nomPilote = nomP);
	return nbVol;
END
/

ACCEPT X prompt 'Donnez le nom dun pilote';
BEGIN
	DBMS_OUTPUT.put_line('Le pilote ' || '&X' || ' a ' || nbVolEffectue('&X') || ' vols !');
END
/
```

---

![[cours_PLSQL.pdf#page=93]]
```SQL
CREATE OR REPLACE FUNCTION nbVolAvion(nomP Pilote.nomPilote%TYPE, numA Avion.numAvion%TYPE) RETURN NUMBER
As
	nbVol NUMBER;
BEGIN
	SELECT count(*) INTO nbVol FROM VOL natural join 
	(SELECT * FROM Pilote WHERE nomPilote = nomP) 
	WHERE numAvion = numA;
	RETURN nbVol;
END;
/
  
```
