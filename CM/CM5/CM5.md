# Systeme D'infromation CM5
### https://celene.univ-orleans.fr/pluginfile.php/885008/mod_resource/content/1/session2.pdf

## Questions :
###  1- Dessiner le MLDR puis donner l’ordre de création des tables et enfin le script de création de la table « Composée ».
1. Enseignant, Categorie
2. Formation, Theme
3. Composée

```sql
CREATE TABLE Composée(
    idFormation NUMBER(5),
    idTheme NUMBER(5),
    Volume NUMBER(10),
    CONSTRAINT PK_Composée PRIMARY KEY (idFormation, idTheme),
    CONSTRAINT FK_Composée_Formation_idFormation FOREIGN KEY (idFormation) REFERENCES Formateur(idFormation),
    CONSTRAINT FK_Composée_Theme_idTheme FOREIGN KEY (idTheme) REFERENCES Theme(idTheme)
);
```

### 2- Donnez le PL/SQL qui pour l’enseignant dont l’idEnseignant est 100 va afficher : « Il y a X thèmes dans la table Theme. qui sont : T1, ...,T_n qui ont comme enseignant Y» sachant que Y est le nom de l’enseignant dont l’idEnseignant est 100. <br> Donc il ne faut pas écrire la lettre Y mais chercher comment il s’appelle dans la table des enseignants. <br> Attention : si l’idEnseignant 100 n’existe pas alors il faut simplement afficher « Pas d’enseignant 100 dans la BDD ». Si X est nul alors on affiche la phrase: «Aucun thème pour l’enseignant Y» . Si X=1 alors on écrit au singulier, c'est-à-dire : pas de « s » et « a » au lieu de « ont ».
```sql
DECLARE
    idEns NUMBER = 100;
    nbThemeEns NUMBER;
    nomEns Enseignant.nomEnseignant%TYPE;
    CURSOR C1 IS SELECT nomTheme FROM Theme WHERE idEnseignant = idEns;
    message VARCHAR(1000);
BEGIN
    SELECT count(idTheme) into nbThemeEns FROM Theme WHERE idEnseignant = idEns;
    SELECT nomEnseignant into nomEns FROM Enseignant WHERE idEnseignant = idEns;
    IF nomEns is NULL THEN
        message = ("Pas d’enseignant 100 dans la BDD");
    message = ("Il y a " || nbThemeEns " thèmes dans la table Theme. qui sont : ");
    FOR nomTheme in C1 LOOP
        message := (message || nomTheme || " ,");
    END LOOP;
    message := (message || " qui ont comme enseignant " || nomEns);
    
    
    dbms_outpout.put_line(message);

END
/


```


### 3- Ecrire une fonction nbThemes (idFormation,IdEnseignant) qui prend en entrée deux identifiants : celui de la formation et celui de l’enseignant et qui renvoie le nombre de thèmes de la formation IdFormation dont l’identifiant de l’enseignant est idEnseignant


### 4- On vous rajoute maintenant la table FormationDupont(IdFormation,nb) : la première colonne est pour l'identifiant de la formation et la deuxième pour le nombre de thèmes dont l'enseignant est « Dupont» (Dupont est le nom de l'enseignant) et qui apparaissent dans cette formation. Par exemple la ligne (4,5) signifie que la formation dont l'IdFormation est 4 contient 5 thèmes dont l'enseignant est Dupont. Supposons que seule la première colonne est remplie et que la seconde colonne est vide. Mettez à jour cette colonne automatiquement via un curseur et des appels à la fonction de l’exercice du point 3 précédent.


### 5- Ecrire un trigger sur la table Theme qui permet d'appliquer cette règle : on envoie un message d'erreur si le thème à supprimer apparaît dans au moins trois formations et que son enseignant est Martin (Martin est le nom de l’enseignant).


### 6- Créez la table Homonyme(nomEnseignant, nbHomonyme) et remplissez la automatiquement sachant que les deux colonnes sont vides au moment de sa création et que votre PL/SQL doit remplir les deux colonnes et gérer tous les noms qui figurent dans la BDD. Par exemple la ligne : ‘dupont’ – 5 signifiera qu’il y a 5 dupont dans la BDD. Bien sure cette ligne ne sera affichée qu’une seule fois dans la table Homonyme et surtout pas 5 fois 
![[CM5_sujet.pdf]]