-- Lire le numéro d’un pilote dans une variable de substitution subP puis déclarez :
--  - une variable local P qui est du même type que numPilote de la table Vol et initialisée à 0
--  - une variable local PiloteNN qui est du même type que P et initialisée à 100
--  - une variable externe Addition qui contient la somme des trois variables ci-dessus
-- Enfin affichez Addition
Accept nomP Prompt 'Donnez le nom du pilote ';
VARIABLE addition NUMBER;
DECLARE 
    P VOL.numPilote%TYPE = 0;
    PiloteNN P%TYPE = 100;
BEGIN 
:addition := &cap+PiloteNN+P;
END;
/
PRINT addition; -- affichage


-- Définir un tableau dans lequel on mettra les numéro de pilote. Vous devez donc définir le
-- même type que celui des numéros de pilote en utilisant%TYPE
-- Affecter à la première case de ce tableau la valeur 12
DECLARE
    TYPE monType IS TABLE OF PILOTE.numPilote%TYPE INDEX BY BINARY_INTEGER;
    maVar monType;
BEGIN
maVar(0) := 12;
END;
/

-- Afficher : lire un numéro de pilote (par exemple 10) puis afficher la phrase :
-- Le numéro 10 correspond à Dupont
Accept _numP Prompt 'num Pilote';
DECLARE
    nomP PILOTE.nomPilote%TYPE;
BEGIN
    Select nomPilote into nomP FROM PILOTE where numP=&_numP;
    dbms_outpout.put_line('Le numéro ' || &_numP || ' correspond à ' || nomP);
END;
/

-- Reprendre l’exercice précédent mais avec un ROWTYPE
-- Afficher : lire un numéro de pilote (par exemple 10) puis afficher la phrase :
-- Le numéro 10 correspond à Dupont
Accept _numP Prompt 'num Pilote';
DECLARE
    -- TYPE Pilote IS TABLE OF PILOTE%TYPE INDEX BY BINARY_INTEGER;
    un_Pilote Pilote%ROWTYPE;
BEGIN
    Select * into un_Pilote from PILOTE where numP = &_numP;
    dbms_outpout.put_line('Le numéro ' || &_numP || ' correspond à ' || un_Pilote.nomPilote)
END;
/

-- Lire un numpilote X et une ville Y puis trouver où habite X et enfin afficher la phrase :
-- Le [nom du pilote X] qui habite [adresse de X] a fait Z vols vers la ville Y.
-- Par exemple :
-- Le pilote Dupont qui habite Orleans a fait 5 vols vers la ville Nice.
Accept numP Prompt 'num Pilote';
Accept ville Prompt 'ville Destination';
DECLARE
    unPilote Pilote%ROWTYPE;
    nbVol NUMBER;
BEGIN
    Select * into unPilote FROM PILOTE where numPilote = &numP;
    Select count(numVol) into nbVol FROM VOL where numPilote = &numP and villeArr='&ville';
    dbms_outpout.put_line('Le pilote ' || unPilote.nomPilote ||' qui habite ' || unPilote.adresse ||
' a fait ' || nbVol || ' vols vers la ville ' || '&ville');
END;
/
