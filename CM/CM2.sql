-- Ecrire un programme qui lit le nom d’un pilote et affiche toutes ses informations.
Accept nomP Prompt 'Donner le nom';
Select *
From Pilote
where nomPilote = & nomP;
-- Ecrire un programme qui affiche tous les avions qui ont fait au moins un vol mais qui sont
-- jamais allait à la ville X avec le pilote Y où X et Y sont des variables de sub qui représentent
-- le nom de la ville d’arrivée et le nom du pilote.
Accept X Prompt 'Donner le nom dune ville ';
Accept Y Prompt 'Doner le nom dun pilote';
Select numAvion,
    From Avion
minus
(
    Select numAvion
    From Vol
        natural join Avion
        natural join Pilote
    where villeArr = '&X'
        and nomPilote = '&Y'
);
-- reponse
Accept nomP Prompt 'Donnez le nom du pilote ';
Accept nomV Prompt 'Donnez le nom de la ville ';
Select numAvion
From Vol
MINUS
(
    select numAvion
    from (
            (
                select numPilote
                from Pilote
                where nomPilote = '&nomP'
            )
            natural join (
                select *
                from Vol
                where villeArr = '&nomV'
            )
        )
);
