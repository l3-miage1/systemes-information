-- DROP
DROP TABLE EnseignantSansContrat; -- pas oublier de la recree apres au cas ou
DROP TABLE Formation CASCADE CONSTRAINT;
DROP TABLE Contrats;
DROP TABLE Competence;
DROP TABLE Formateur;


-- CREATE
CREATE TABLE Formateur
(
    id_Enseignant NUMBER(8) CONSTRAINT PK_Formateur PRIMARY KEY,
    nom VARCHAR2(50),
    prenom VARCHAR2(50),
    date_naiss DATE,
    salaire NUMBER(6)
);
CREATE TABLE Competence
(
    id_Enseignant NUMBER(8),
    matiere VARCHAR2(25),
    CONSTRAINT PK_Competence PRIMARY KEY (id_Enseignant, matiere),
    CONSTRAINT FK_Competence_Formateur_id_Enseignant FOREIGN KEY (id_Enseignant) REFERENCES Formateur(id_Enseignant)
);
CREATE TABLE Contrats
(
    id_Enseignant NUMBER(8),
    societe VARCHAR2(55),
    CONSTRAINT PK_Contrats PRIMARY KEY (id_Enseignant, societe),
    CONSTRAINT FK_Contrats_Formateur_id_Enseignant FOREIGN KEY (id_Enseignant) REFERENCES Formateur(id_Enseignant)
);
-- enfaite ces attributs la ne vienne pas des autres tables.
CREATE TABLE Formation
(
    matiere VARCHAR2(25),
    societe VARCHAR2(25),
    CONSTRAINT PK_Formation PRIMARY KEY (matiere, societe)
);



-- INSERT
-- insertion des formateurs
INSERT INTO Formateur VALUES (1, 'Dupont', 'Jean', TO_DATE('01/01/1980', 'DD/MM/YYYY'), 1000);
INSERT INTO Formateur VALUES (2, 'Durand', 'Pierre', TO_DATE('01/01/1985', 'DD/MM/YYYY'), 1000);
INSERT INTO Formateur VALUES (3, 'Martin', 'Paul', TO_DATE('01/01/1990', 'DD/MM/YYYY'), 1000);
INSERT INTO Formateur VALUES (4, 'Dubois', 'Jacques', TO_DATE('01/01/1995', 'DD/MM/YYYY'), 1000);
INSERT INTO Formateur VALUES (5, 'Lefebvre', 'Jeanne', TO_DATE('01/01/2000', 'DD/MM/YYYY'), 1000);
INSERT INTO Formateur VALUES (6, 'Lenoir', 'Mélanie', TO_DATE('01/01/2005', 'DD/MM/YYYY'), 1000);
INSERT INTO Formateur VALUES (7, 'Leblanc', 'Herve', TO_DATE('01/01/2005', 'DD/MM/YYYY'), 1000);

-- insertion des compétences
INSERT INTO Competence VALUES (1, 'ORACLE');
INSERT INTO Competence VALUES (1, 'JAVA');
INSERT INTO Competence VALUES (1, 'C++');
INSERT INTO Competence VALUES (2, 'ORACLE');
INSERT INTO Competence VALUES (2, 'JAVA');
INSERT INTO Competence VALUES (3, 'ORACLE');
INSERT INTO Competence VALUES (3, 'C++');
INSERT INTO Competence VALUES (4, 'JAVA');
INSERT INTO Competence VALUES (4, 'C++');
INSERT INTO Competence VALUES (5, 'ORACLE');
INSERT INTO Competence VALUES (5, 'JAVA');
INSERT INTO Competence VALUES (5, 'C++');
INSERT INTO Competence VALUES (5, 'PYTHON');
INSERT INTO Competence VALUES (6, 'ORACLE');
INSERT INTO Competence VALUES (6, 'JAVA');
INSERT INTO Competence VALUES (6, 'C++');
INSERT INTO Competence VALUES (6, 'PYTHON');

-- insertion des contrats
INSERT INTO Contrats VALUES (1, 'IBM');
INSERT INTO Contrats VALUES (1, 'ORACLE');
INSERT INTO Contrats VALUES (2, 'IBM');
INSERT INTO Contrats VALUES (2, 'ORACLE');
INSERT INTO Contrats VALUES (3, 'IBM');
INSERT INTO Contrats VALUES (3, 'ORACLE');
INSERT INTO Contrats VALUES (4, 'IBM');
INSERT INTO Contrats VALUES (4, 'ORACLE');
INSERT INTO Contrats VALUES (5, 'IBM');
INSERT INTO Contrats VALUES (5, 'ORACLE');
INSERT INTO Contrats VALUES (6, 'IBM');
INSERT INTO Contrats VALUES (6, 'ORACLE');
INSERT INTO Contrats VALUES (6, 'PYTHON');
INSERT INTO Contrats VALUES (6, 'PYTHONS');
INSERT INTO Contrats VALUES (6, 'PYTHONSS');
INSERT INTO Contrats VALUES (6, 'PYTHONSSS');
INSERT INTO Contrats VALUES (6, 'PYTHONSSSS');
INSERT INTO Contrats VALUES (6, 'PYTHONSSSSS');
INSERT INTO Contrats VALUES (6, 'PYTHONSSSSSS');
INSERT INTO Contrats VALUES (6, 'PYTHONSSSSSSS');
INSERT INTO Contrats VALUES (6, 'PYTHONSSSSSSSS');
INSERT INTO Contrats VALUES (6, 'PYTHONSSSSSSSSS');
INSERT INTO Contrats VALUES (6, 'PYTHONSSSSSSSSSS');
INSERT INTO Contrats VALUES (6, 'PYTHONSSSSSSSSSSS');
INSERT INTO Contrats VALUES (6, 'PYTHONSSSSSSSSSSSS');
INSERT INTO Contrats VALUES (6, 'PYTHONSSSSSSSSSSSSS');
INSERT INTO Contrats VALUES (6, 'PYTHONSSSSSSSSSSSSSS');


-- insertion des formations
INSERT INTO Formation VALUES ('ORACLE', 'IBM');
INSERT INTO Formation VALUES ('JAVA', 'IBM');
INSERT INTO Formation VALUES ('C++', 'IBM');
INSERT INTO Formation VALUES ('ORACLE', 'ORACLE');
INSERT INTO Formation VALUES ('JAVA', 'ORACLE');
INSERT INTO Formation VALUES ('C++', 'ORACLE');
INSERT INTO Formation VALUES ('PYTHON', 'ORACLE');
INSERT INTO Formation VALUES ('ORACLE', 'MICROSOFT');
INSERT INTO Formation VALUES ('JAVA', 'MICROSOFT');
INSERT INTO Formation VALUES ('C++', 'MICROSOFT');
INSERT INTO Formation VALUES ('PYTHON', 'MICROSOFT');
INSERT INTO Formation VALUES ('.NET', 'MICROSOFT');