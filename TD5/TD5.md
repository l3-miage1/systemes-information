u# TD5
## PL/SQL : Extension pour SQL (Language Procédural)
- Variable de substitution -> initialisation en dehors du PL/SQL


### Etape pr l'exo 1
- Suppression des tables -> creation_table.sql
- Création des tables -> creation_table.sql
- Insertion d'un jeu de données -> creation_table.sql
- lire(X)
- Z : entier
Z <- CompterNombresContrats(X)
Afficher (X a signé Z contrats) 

## Model PL/SQL
- PL/SQL
    - DECLARE (optionel)
    - BEGIN
    - EXCEPTION (optionel)
    - END;
    - /

## Exercice

### 1.1 - Lire le nom X (pas son identifiant) d’un formateur puis afficher la phrase : le formateur X a  signé  des contrats avec Z sociétés. (où Z représente le nombre total de contrats signés entre X et les sociétés de votre BDD).
```SQL
ACCEPT X PROMPT 'message';
SET SERVEROUTPUT ON;
DECLARE
    Z NUMBER(30);
BEGIN
    SELECT count(*) INTO Z FROM Contrats WHERE  id_Enseignant=(
        SELECT id_Enseignant FROM Formateur WHERE nom='&X'
    );
    DBMS_OUTPUT.PUT_LINE('Le formateurs ' || '&X' || ' a signé des contrats avec ' || Z || ' société');
END;
/
```
#### Marche sur liveSQL oracle
```SQL
DECLARE
    X VARCHAR2(255) := "Durand";
    Z NUMBER(30);
BEGIN
    SELECT count(*) INTO Z FROM Contrats WHERE id_Enseignant=(
        SELECT id_Enseignant FROM Formateur WHERE nom=X
    );
    DBMS_OUTPUT.PUT_LINE('Le formateurs ' || X || ' a signé des contrats avec ' || Z || ' société');
END;
/
```

## QUESTION 1.2 
* Modifier pour que : <br> 
    * si Z=0 alors on affiche « aucun contrat signé de la part de X »,
    * si Z=1 on affiche « un seul contrat signé par X »,
    * dans tous les autres cas on affiche la phrase de l’exercice précédent. 
```SQL
ACCEPT X PROMPT 'message';
SET SERVEROUTPUT ON;
DECLARE
    Z NUMBER(30);
BEGIN
    SELECT count(*) INTO Z FROM Contrats WHERE  id_Enseignant=(
        SELECT id_Enseignant FROM Formateur WHERE nom='&X'
    );
    IF Z=0 THEN
        DBMS_OUTPUT.PUT_LINE('Auncun contrats signé de la part de ' || '&X');
    ELSIF Z=1 THEN
        DBMS_OUTPUT.PUT_LINE('Un seul contrats signé de la part de ' || '&X');
    ELSE
        DBMS_OUTPUT.PUT_LINE('Le formateurs ' || '&X' || ' a signé des contrats avec ' || Z || ' société');
    END IF;
END;
/
```
#### Marche sur liveSQL oracle
```SQL
DECLARE
    nomX VARCHAR(50) := 'Charles';
    Z NUMBER(30);
BEGIN
    SELECT count(*) INTO Z FROM Contrats WHERE  id_Enseignant=(
        SELECT id_Enseignant FROM Formateur WHERE nom = nomX
    );
    IF Z=0 THEN
        DBMS_OUTPUT.PUT_LINE('Auncun contrats signé de la part de ' || nomX);
    ELSIF Z=1 THEN
        DBMS_OUTPUT.PUT_LINE('Un seul contrats signé de la part de ' || nomX);
    ELSE
        DBMS_OUTPUT.PUT_LINE('Le formateurs ' || nomX || ' a signé des contrats avec ' || Z || ' société');
    END IF;
END;
/
```
## QUESTION 1.3
* En utilisant un curseur modifiez pour que :
    * Si Z=1 il affiche : «un seul contrat signé par X avec la société W » où W est l’identifiant de la société avec laquelle X a signé.
    * Si Z est supérieur à un il affiche :  « le formateur X a signé  des contrats avec Z sociétés qui sont : W1 – W2 – W3 – W4 - ... - Wn » où W1..Wn sont les identifiants des sociétés avec lesquelles X a signé.
    * **AIDE** : cours_PLSQL.pdf page 64 => *Le curseur explicite*
#### Marche sur liveSQL oracle
```SQL
DECLARE
    nomX VARCHAR(50) := 'Durand';
    Z NUMBER(30);
    CURSOR C1 IS SELECT societe FROM Contrats WHERE id_Enseignant=(
        SELECT id_Enseignant FROM Formateur WHERE nom = nomX
    );
    mess VARCHAR2(255);
    
BEGIN
    SELECT count(*) INTO Z FROM Contrats WHERE  id_Enseignant=(
        SELECT id_Enseignant FROM Formateur WHERE nom = nomX
    );
    IF Z =1 THEN
        FOR cursor_contrat_EnsX in C1 LOOP -- faire un for vraiment utile ?
        	mess := ('un seul contrat signé par ' || nomX || ' avec la société ' || cursor_contrat_EnsX.societe);
		END LOOP;
    ELSIF Z >1 THEN
        mess := ('le formateur ' || nomX || ' a signé des contrats avec ' || Z || ' sociétés qui sont : ');
        FOR cursor_contrat_EnsX in C1 LOOP
            mess := (mess || cursor_contrat_EnsX.societe || ' - '); -- fini par un '-' comment l'enlever ?
        END LOOP;
        mess := (mess || ' sont les identifiants des sociétés avec lesquelles ' || nomX || ' a signé.');
	ELSE
        mess := ('Aucun');
    END IF;
    DBMS_OUTPUT.PUT_LINE(mess);
END;
/
```

## QUESTION 2 
* Créez une table EnseignantSansContrat(id_Enseignant) puis rajouter au point précédent ce qu’il faut pour que pour chaque enseignant de la base on affiche la phrase du point précédent et dans le cas où cet enseignant n’a aucun contrat on le rajoute également dans la table EnseignantSansContrat.

> Faire le qui marche hypothétiquement sur l'autre
#### Marche sur liveSQL oracle
* Si le nomX est incorrect tout plante 
	* Soit revenir pour régler le problème
	* Ou supposer aucune erreur d'entrée
```SQL
DROP TABLE EnseignantSansContrat;
CREATE TABLE EnseignantSansContrat(
	id_Enseignant NUMBER(8) CONSTRAINT PK_EnseignantSansContrat PRIMARY KEY,
    CONSTRAINT FK_EnseignantSansContrat_Formateur_id_Enseignant FOREIGN KEY (id_Enseignant) REFERENCES Formateur(id_Enseignant)
);
DECLARE
    nomX VARCHAR(50) := 'Leblanc';
    Z NUMBER(30);
    id_ens Formateur.id_Enseignant%TYPE;
    CURSOR societeContratAvecNom IS SELECT societe FROM Contrats WHERE id_Enseignant=(
        SELECT id_Enseignant FROM Formateur WHERE nom = nomX
    );
    mess VARCHAR2(255);
    
BEGIN
    SELECT count(*) INTO Z FROM Contrats WHERE  id_Enseignant=(
        SELECT id_Enseignant FROM Formateur WHERE nom = nomX
    );
	SELECT id_Enseignant INTO id_ens FROM Formateur WHERE nom = nomX;
    IF Z =1 THEN
        FOR cursor_contrat_EnsX in societeContratAvecNom LOOP -- faire un for vraiment utile ?
        	mess := ('un seul contrat signé par ' || nomX || ' avec la société ' || cursor_contrat_EnsX.societe);
		END LOOP;
    ELSIF Z >1 THEN
        mess := ('le formateur ' || nomX || ' a signé des contrats avec ' || Z || ' sociétés qui sont : ');
        FOR cursor_contrat_EnsX in societeContratAvecNom LOOP
            mess := (mess || cursor_contrat_EnsX.societe || ' - '); -- fini par un '-' comment l'enlever ?
		END LOOP;
        mess := (mess || ' sont les identifiants des sociétés avec lesquelles ' || nomX || ' a signé.');
	ELSIF Z =0 THEN
        
        INSERT INTO EnseignantSansContrat values (id_ens);
		mess := 'Aucun contrat signé avec ' || nomX;
    END IF;
    DBMS_OUTPUT.PUT_LINE(mess);
END;
/
SELECT * FROM EnseignantSansContrat;
```

## QUESTION 3
* Supposons que l’on dispose d’une colonne salaire dans la table Enseignant qui contient le salaire de chaque enseignant. Supposons que vous venez de recevoir un ordre d’augmentation des salaires selon la règle ci-dessous. Donnez le bloc PL/SQL qui permet de mettre à jour le salaire de tous vos enseignants.
    * 10 % pour ceux qui ont fait plus de 10 contrats
    * 20 % pour ceux qui ont fait plus de 20 contrats
    * 30 % pour ceux qui ont fait plus de 30 contrats 
    * et -10 % pour ceux qui ont pas fait de contrats.

#### Marche sur liveSQL oracle
```SQL
DECLARE
    nombreContratsEmployeX NUMBER(30);
	nouveauSalaire Formateur.salaire%TYPE;
    CURSOR C1 IS SELECT id_Enseignant, salaire FROM Formateur;   
BEGIN
    FOR enseignant in C1 LOOP -- faire un for vraiment utile ?
        SELECT count(*) INTO nombreContratsEmployeX FROM Contrats WHERE  id_Enseignant = enseignant.id_Enseignant;
		IF 10 < nombreContratsEmployeX AND nombreContratsEmployeX <= 20 THEN
            nouveauSalaire := enseignant.salaire + (enseignant.salaire*0.10);
			UPDATE Formateur SET salaire=nouveauSalaire WHERE id_Enseignant= enseignant.id_Enseignant;
        ELSIF 20 < nombreContratsEmployeX AND nombreContratsEmployeX <= 30 THEN
            nouveauSalaire := enseignant.salaire + (enseignant.salaire*0.20);
			UPDATE Formateur SET salaire=nouveauSalaire WHERE id_Enseignant= enseignant.id_Enseignant;
        ELSIF 30 < nombreContratsEmployeX THEN
            nouveauSalaire := enseignant.salaire + (enseignant.salaire*0.30);
		ELSE
            nouveauSalaire := enseignant.salaire; -- Sinon il peut y avoir des probleme de gens qui on pas fait assez de contrat qui se retrouve avec un salaire qui augmente
        END IF;
		IF nouveauSalaire <> enseignant.salaire THEN
			DBMS_OUTPUT.PUT_LINE('Enseignant -> ' || enseignant.id_Enseignant || ' mis a jours : Ancien Salaire ' || enseignant.salaire || ' | Nouveau Salaire ' || nouveauSalaire ||'.   Nb contrats ' || nombreContratsEmployeX);
            UPDATE Formateur SET salaire=nouveauSalaire WHERE id_Enseignant= enseignant.id_Enseignant;
		END IF;
	END LOOP;
END;
/
SELECT id_Enseignant, salaire FROM Formateur;
```

## QUESTION 4
* Ecrire un curseur paramétré par (X,Y,Z) où X est l’identifiant de la société et Y et Z deux matières. 
* Ce curseur affiche le nombre et la liste des noms des enseignants qui sont compétents dans la matière Y mais pas en Z et qui ont signé un contrat avec la société X 
* puis utilisez le pour lire deux identifiants de sociétés X1 et X2 et afficher pour chacune la liste des noms des enseignants compétents en ORACLE mais pas en JAVA qui ont signé un contrat avec elle.
* Enfin afficher l’identifiant de celle qui a le plus de contrats : « La société X1 a plus de contrats que la société X2 ». 

#### Marche sur liveSQL oracle
```SQL
-- ACCEPT X1 PROMPT 'identifiant de la société';
-- ACCEPT X2 PROMPT 'identifiant de la société';
-- ACCEPT Y PROMPT 'matiere possible';
-- ACCEPT Z PROMPT 'matiere non possible';
DECLARE
    X1 Contrats.societe%TYPE := 'IBM';
    X2 Contrats.societe%TYPE := 'PYTHON';
    X X1%TYPE := X1;
    Y Competence.matiere%TYPE := 'ORACLE';
    Z Competence.matiere%TYPE := 'JAVA';
    mess VARCHAR2(255);
    nbContratsX1 NUMBER;
    nbContratsX2 NUMBER;
    CURSOR C1 IS SELECT nom FROM Formateur natural join (
        SELECT id_Enseignant FROM Contrats WHERE societe=X
    ) 
    natural join (
        SELECT id_Enseignant FROM Competence
        WHERE matiere=Y and id_Enseignant not in (
            SELECT id_Enseignant FROM Competence WHERE matiere=Z
        )
    );
BEGIN
    mess := 'Le nom des enseignant compétent en '|| Y ||' mais pas en '|| Z ||' dans lentreprise '|| X ||' : ';
    FOR enseignant in C1 LOOP -- faire un for vraiment utile ?
        mess := mess || enseignant.nom ||', ';
    END LOOP;
    DBMS_OUTPUT.put_line(mess);
    
    X := X2;
    mess := 'Le nom des enseignant compétent en '|| Y ||' mais pas en '|| Z ||' dans lentreprise '|| X ||' : ';
    FOR enseignant in C1 LOOP -- faire un for vraiment utile ?
        mess := mess || enseignant.nom ||', ';
    END LOOP;
    DBMS_OUTPUT.put_line(mess);

    SELECT COUNT(*) INTO nbContratsX1 FROM Contrats WHERE societe=X1;
    SELECT COUNT(*) INTO nbContratsX2 FROM Contrats WHERE societe=X2;
    IF nbContratsX1>nbContratsX2 THEN
         DBMS_OUTPUT.put_line('La société '||X1 ||' à plus de contrats que la société ' ||X2);
    ELSIF nbContratsX1<nbContratsX2 THEN
         DBMS_OUTPUT.put_line('La société '||X2 ||' à plus de contrats que la société '||X1);
    ELSE 
        DBMS_OUTPUT.put_line('Les 2 société '||X2||' et '||X1||' ont autant de contrats : '||nbContratsX1);
	END IF;
END;
/
```


### A rendre sur feuille
```SQL
ACCEPT X1 PROMPT 'identifiant d une société';
ACCEPT X2 PROMPT 'identifiant d une autre société';
ACCEPT Y PROMPT 'matiere possible';
ACCEPT Z PROMPT 'matiere non possible';
DECLARE
    X Contrats.societe%TYPE := '&X1';
    mess VARCHAR2(255);
    nbContratsX1 NUMBER;
    nbContratsX2 NUMBER;
    -- CURSOR C1(X, Y, Z) IS SELECT 
    CURSOR C1 IS SELECT nom FROM Formateur natural join (
        SELECT id_Enseignant FROM Contrats WHERE societe=X
    ) 
    natural join (
        SELECT id_Enseignant FROM Competence
        WHERE matiere='&Y' and id_Enseignant not in (
            SELECT id_Enseignant FROM Competence WHERE matiere='&Z'
        )
    );
BEGIN
    mess := 'Le nom des enseignant compétent en '|| '&Y' ||' mais pas en '|| '&Z' ||' dans lentreprise '|| X ||' : ';
    FOR enseignant in C1 LOOP -- faire un for vraiment utile ?
        mess := mess || enseignant.nom ||', ';
    END LOOP;
    DBMS_OUTPUT.put_line(mess);
    
    X := X2;
    mess := 'Le nom des enseignant compétent en '|| '&Y' ||' mais pas en '|| '&Z' ||' dans lentreprise '|| X ||' : ';
    FOR enseignant in C1 LOOP -- faire un for vraiment utile ?
        mess := mess || enseignant.nom ||', ';
    END LOOP;
    DBMS_OUTPUT.put_line(mess);

    SELECT COUNT(*) INTO nbContratsX1 FROM Contrats WHERE societe='&X1';
    SELECT COUNT(*) INTO nbContratsX2 FROM Contrats WHERE societe='&X2';
    IF nbContratsX1>nbContratsX2 THEN
         DBMS_OUTPUT.put_line('La société '||'&X1' ||' à plus de contrats que la société ' ||'&X2');
    ELSIF nbContratsX1<nbContratsX2 THEN
         DBMS_OUTPUT.put_line('La société '||'&X2' ||' à plus de contrats que la société '||'&X1');
    ELSE 
        DBMS_OUTPUT.put_line('Les 2 société '||'&X2'||' et '||'&X1'||' ont autant de contrats : '||nbContratsX1);
	END IF;
END;
/
```


### CORRECTION
```SQL
ACCEPT X1 PROMPT 'INTRODUISEZ X1';
ACCEPT X2 PROMPT 'INTRODUISEZ X2';
DECLARE
	cpt1 NUMBER :=0;
	cpt2 NUMBER :=0;
	CURSOR Cursor_ENS(X VARCHAR2, Y VARCHAR2, Z VARCHAR2) IS SELECT nom FROM Formateur WHERE id_Enseignant in (SELECT id_Enseignant FROM Contrat WHERE societe=X and id_Enseignant in ( SELECT id_Enseignant FROM Competence WHERE matiere=Y MINUS(SELECT id_Enseignant FROM Competence where matiere=Z)));
BEGIN
	DBMS_OUTPUT.put_line('Liste des enseignants qui ont signé un contrat avec la société ' || '&X1');
	FOR ENS1 in Cursor_ENS('&X1', 'ORACLE', 'JAVA') LOOP
		aqt


```
















